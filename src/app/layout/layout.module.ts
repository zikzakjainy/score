import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './navbar/navbar.component';
import { ThemePaletteComponent } from './theme-palette/theme-palette.component';

@NgModule({
  declarations: [NavbarComponent, ThemePaletteComponent],
  imports: [
    CommonModule
  ]
})
export class LayoutModule { }
