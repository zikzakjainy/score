import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public unit: number = 0;
  public double: number = 0;
  public tripal: number = 0;
  public sum: number = 0;
  public wkt: number = 0;
  public runningOver: number = 0;
  public runningOverBall: number = 0;
  public isThisOver: boolean =true;
  public thisRun: any;
  thisOver: any [] = [];
  runsInThisOver: number = 0;
  isWideRun: boolean = false;
  isNbRun: boolean = false;
  isDisplayRules: boolean = false;
  history: any;
  currentInning = 1;
  gameNo: number = 1;
  updateRunsData: any [] = [];
  isWantToUpdate: boolean;
  constructor() {
   }
  changeUnit(val) {
    this.unit = val;
    this.totalScore();
    this.inThisOver(val);
  }
  changeDouble(val) {
    this.double = val;
    this.totalScore();
    this.inThisOver(val);
  }
  changeTripal(val) {
    this.tripal = val;
    this.totalScore();
    this.inThisOver(val);
  }
  totalScore() {
    const data = this.tripal + '' + this.double + '' + this.unit;
    this.sum = +data;
  }
  ngOnInit() {
  }
  //////////////////////////// LOGIC TWO ////////////////////////////////////////////
  updateTotal(val) {
    this.thisRun = val;
    this.inThisOver(val);
    if ( typeof val === 'number' ) {
      this.sum += val;
      this.runsInThisOver += val;
      this.runningOverBall++;
      this.checkIsOverEnd();
    } else {
      if (val === 'Wkt') {
        this.wkt++;
        this.runningOverBall++;
        this.checkIsOverEnd();
      }
      if (this.isWideRun && val === 'Wd') {
        this.sum += 1;
        this.runsInThisOver += 1;
      }
      if (this.isNbRun && val === 'Nb') {
        this.sum += 1;
        this.runsInThisOver += 1;
      }
    }
  }
  checkIsOverEnd () {
    if (this.runningOverBall === 6) {
      this.endOver();
    }
  }
  toggleWideRun() {
    this.isWideRun = !(this.isWideRun);
   // console.log('toggleWideRun ', this.isWideRun);
   // this.isDisplayRules = false;
  }
  toggleNbRun() {
    this.isNbRun = !(this.isNbRun);
   // console.log('toggleNbRun ', this.isNbRun);
   // this.isDisplayRules = false;
  }
  toggle() {
    this.isThisOver = !(this.isThisOver);
  }
  inThisOver(val) {
    let shedowVal = val;
    if (this.isNbRun && val === 'Nb') {
      shedowVal = 'Nb + 1';
    }
    if (this.isWideRun && val === 'Wd') {
      shedowVal = 'Wd + 1';
    }
    this.thisOver.push(shedowVal);
  }
  endOver() {
    const result = confirm('OVER END ?');
    if (result) {
      this.runningOver++;
      const currentOver = 'over - ' + this.runningOver;
      const overStatus = this.thisOver;
      const overDetails = {over : currentOver, runs : overStatus};
      if (localStorage.getItem(`Game - ${this.gameNo} / Inning - ${this.currentInning}`)) {
        const updateRunningInning = JSON.parse(localStorage.getItem(`Game - ${this.gameNo} / Inning - ${this.currentInning}`));
        const overArray = updateRunningInning;
        overArray.unshift(overDetails);
        localStorage.setItem(`Game - ${this.gameNo} / Inning - ${this.currentInning}`, JSON.stringify(overArray));
      } else {
        const overArray = [];
        overArray.unshift(overDetails);
        localStorage.setItem(`Game - ${this.gameNo} / Inning - ${this.currentInning}`, JSON.stringify(overArray));
      }
      this.thisOver = [];
      this.updateRunsData = [];
      this.runsInThisOver = 0;
      this.runningOverBall = 0;
    }
  }
  endInning() {
    const result = confirm('OVER Inning ?');
    if (result) {
      const ininngTotal = `${this.sum}/${this.wkt}`;
      localStorage.setItem(`Total : Game - ${this.gameNo} / Inning - ${this.currentInning}`, ininngTotal);
      this.sum = 0;
      this.wkt = 0;
      this.thisOver = [];
      this.runsInThisOver = 0;
      this.runningOver = 0;
      this.runningOverBall = 0;
      this.currentInning++;
      if (this.currentInning === 3) {
        const isGameEnd = confirm('Is Game End ?');
        if (isGameEnd) {
          const inning1Score = localStorage.getItem(`Total : Game - ${this.gameNo} / Inning - ${this.currentInning - 2}`);
          const inning2Score = localStorage.getItem(`Total : Game - ${this.gameNo} / Inning - ${this.currentInning - 1}`);
          const score1arr = inning1Score.split('/');
          const score1 = score1arr[0];
          const score2arr = inning2Score.split('/');
          const score2 = score2arr[0];
          if (+(score1) > +(score2)) {
            alert(' Inning 1 team won the game');
          }
          if (+(score1) < +(score2)) {
            alert(' Inning 2 team won the game');
          }
          if (+(score1) === +(score2)) {
            alert(' Match Tie, ready for Super over');
          }
          this.gameNo++;
          this.resetData();
        }
      }
    }
  }
  resetData() {
    this.sum = 0;
    this.wkt = 0;
    this.runningOver = 0;
    this.runningOverBall = 0;
    this.isThisOver = true;
    this.thisRun = undefined;
    this.thisOver = [];
    this.runsInThisOver = 0;
    this.isWideRun = false;
    this.isNbRun = false;
    this.isDisplayRules = true;
    this.currentInning = 1;
  }
  getHistory() {
    const local: any = JSON.stringify(localStorage);
    const localObj = JSON.parse(local);
    this.history = localObj;
  }
  dispalyRules() {
    this.isDisplayRules = !(this.isDisplayRules);
  }
  clearFullHistory() {
    const result = confirm('Want to Clear full HISTORY ?');
    if (result) {
      localStorage.clear();
      this.isThisOver = true;
    }
  }
  //////////////// Update Over ///////////////
  onUpdatingRuns(event, index) {
    const notNumericValue = isNaN(+(event.target.value));
    let value;
    if (notNumericValue) {
      value = event.target.value;
    } else {
      value = +(event.target.value);
    }
   // console.log(`index ${index} , value ${value}`);
    const findInd = this.updateRunsData.findIndex((val) => val.ind === index);
    if (findInd === -1) {
      this.updateRunsData.push({'ind': index, 'value' : value});
    } else {
      this.updateRunsData[findInd].value = value;
    }
   // console.log('findInd, ', findInd);
   // console.log('updateRunsData, ', this.updateRunsData);
  }
  changeOverRuns() {
    const result = confirm('Save Changes ?');
    if (result) {
      const currentOverData = [... this.thisOver];
    // console.log('currentOverData before ', currentOverData);
    const thisCurrentOverRun: number = this.runsInThisOver;
    let updatedRunsInThisOver = 0;
    if (this.updateRunsData.length > 0) {
      this.updateRunsData.forEach((data) => {
        const updateInd = data.ind;
        const updatedVal = data.value;
        if (currentOverData[updateInd] === 'Wkt') {
            this.wkt--;
            currentOverData[updateInd] = updatedVal;
            if (updatedVal === 'Wkt') {
              this.wkt++;
            }
        } else {
            currentOverData[updateInd] = updatedVal;
            if (updatedVal === 'Wkt') {
              this.wkt++;
            }
        }
        // currentOverData[updateInd] = updatedVal;
      });
    // console.log('currentOverData after ', currentOverData);
      currentOverData.forEach((val) => {
        if ( typeof val === 'number' ) {
          updatedRunsInThisOver += val;
        } else {
          if (val === 'Wd + 1') {
            updatedRunsInThisOver++;
          }
          if (val === 'Nb + 1') {
            updatedRunsInThisOver++;
          }
        }
      });
      const diffrenceInRuns: number = updatedRunsInThisOver - thisCurrentOverRun;
     // console.log('diffrenceInRuns ', diffrenceInRuns);
      this.sum = this.sum + diffrenceInRuns;
      this.runsInThisOver = updatedRunsInThisOver;
      this.thisOver = currentOverData;
      }
      this.cancelWantToUpdate();
    } else {
      this.cancelWantToUpdate();
    }
  }
  onWantToUpdate() {
    const result = confirm('This features is under Testing, want to use ?');
    if (result) {
      this.isWantToUpdate = true;
    }
  }
  cancelWantToUpdate() {
    this.isWantToUpdate = false;
  }
}
